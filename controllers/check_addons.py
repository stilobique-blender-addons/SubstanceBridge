import bpy
import addon_utils


def check_multi(module_name):
    datas = []
    if module_name == 'Multi-Object-UV-Editing':
        check = addon_utils.check('Multi-Object-UV-Editing')
        datas.append(check)
        text = 'Multi UV object Editing install'
        datas.append(text)
        icon = 'FILE_TICK'
        datas.append(icon)

    elif module_name == 'io_scene_gltf2':
        check = addon_utils.check('io_scene_gltf2')
        datas.append(check)
        text = 'GLTF module install'
        datas.append(text)
        icon = 'FILE_TICK'
        datas.append(icon)

    else:
        check = False
        datas.append(check)
        text = 'Addon not install'
        datas.append(text)
        icon = 'CANCEL'
        datas.append(icon)

    print('Function check > ', datas)

    return datas


# -----------------------------------------------------------------------------
# Function Debug, show many properties
# -----------------------------------------------------------------------------
class SBCheckAddon(bpy.types.Operator):
    """This operator is a simple check if the add-on Multi UV editing has
    present or not."""
    bl_idname = "debug.check_addon"
    bl_label = "A function to check an add-on"

    module_name = ''

    def execute(self, context):
        print('Check Add-ons')
        check = check_multi(self.module_name)

        if check[0]:
            self.report({'INFO'}, "Add-on valid and loaded.")

        else:
            self.report({'WARNING'}, "Add-on not loaded.")

        return {'FINISHED'}, check


class SBCheckMultiUV(SBCheckAddon):
    bl_idname = "debug.check_multi_uv"
    bl_label = "A function to check the add-on Multi UV"
    module_name = 'Multi-Object-UV-Editing'
    print('Check Multi UV')


class SBCheckGLTF(SBCheckAddon):
    bl_idname = "debug.check_gltf"
    bl_label = "A function to check the add-on GLTF"
    module_name = 'io_scene_gltf2'
    print('GLTF')


def register():
    bpy.utils.register_class(SBCheckAddon)
    bpy.utils.register_class(SBCheckMultiUV)
    bpy.utils.register_class(SBCheckGLTF)


def unregister():
    bpy.utils.unregister_class(SBCheckAddon)
    bpy.utils.unregister_class(SBCheckMultiUV)
    bpy.utils.unregister_class(SBCheckGLTF)
